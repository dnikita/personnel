/**
 * Created by ndyumin on 25.09.2015.
 */

var express = require('express');
var app = express();
var winston = require('winston');
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {'timestamp':true});
winston.level = 'debug';

var orm = require('./dao/orm');
orm.connect('./resources/staff.db');

require('./service')(app);

var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;
    winston.log('info', 'listening at http://%s:%d', host, port);
});