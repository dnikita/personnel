/**
 * Created by ndyumin on 01.10.2015.
 */

function Cache() {
    this.cache = {};
}

Cache.prototype = {
    put: function (q, result) {
        this.cache[q] = result;
    },
    get: function (q) {
        if (typeof this.cache[q] !== 'undefined') {
            return this.cache[q];
        }
    },
    invalidate: function() {
        this.cache = {};
    }
};

module.exports = Cache;