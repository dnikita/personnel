/**
 * Created by ������ on 26.09.2015.
 */

var factory = require('./crud.controller.factory');
var Conflict = require('../error/types/Conflict');

var Departments = require('../dao/orm').models.Departments;
var Employees = require('../dao/orm').models.Employees;

module.exports = {
    createOne: factory.getCreateOne(Departments),
    updateOne: factory.getUpdateOne(Departments),
    deleteOne: function(req, res, next) {
        Employees.find({
            "DeptID": {
                "$eq": req.params.id
            }
        }).then(function(empl) {
            if (empl.length === 0) {
                Departments.findById(req.params.id)
                    .then(function (model) {
                        return model.remove();
                    })
                    .then(factory.sendJson(res))
                    .catch(next);
            } else {
                throw new Conflict('department is not empty');
            }
        }).catch(next);
    },
    getOne: factory.getListOne(Departments),
    listAll: factory.getListAll(Departments),
    getDepartmentsWithEmployeeCount: function (req, res, next) {
        Employees.findAggregated({
            count: true,
            aggregate: ['DeptID'],
            populate: {
                field: 'DeptID',
                model: Departments
            }
        }).then(factory.sendJson(res)).catch(next);
    }
};