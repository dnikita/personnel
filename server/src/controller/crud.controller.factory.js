/**
 * Created by ������ on 26.09.2015.
 */

var BadRequest = require('../error/types/BadRequest');
var GenericError = require('../error/types/GenericInternalError');

function toJson(data) {
    if (!data) return data;
    return data instanceof Array
        ? data.map(toJson)
        : data.toJson
            ? data.toJson()
            : data;
}

function sendJson(res) {
    return function(s) {
        return new Promise(function(resolve, reject) {
            try {
                return resolve(res.send(toJson(s)));
            } catch(e) {
                return reject(new GenericError(e.message));
            }
        });
    };
}

function parseQuery(query) {
    var whereClause, orderClause, fieldsClause;
    try {
        if (query.where) {
            whereClause = JSON.parse(query.where);
        }
        if (query.order) {
            orderClause = JSON.parse(query.order);
        }
        if (query.fields) {
            fieldsClause = JSON.parse(query.fields);
        }
    } catch (e) {
        throw new BadRequest(e);
    }

    return {
        whereClause: whereClause || {},
        orderClause: orderClause || [],
        fieldsClause: fieldsClause || []
    };
}

module.exports = {
    toJson: toJson,
    sendJson: sendJson,
    parseQuery: parseQuery,

    getListAll: function(Model) {
        return function(req, res, next) {
            var query = parseQuery(req.query);
            var data = Model.find(query.whereClause, {
                offset: req.query.offset,
                limit: req.query.limit,
                order: query.orderClause
            });
            var count = Model.count(query.whereClause);
            Promise.all([data, count])
                .then(function(result) {
                    res.send({
                        data: toJson(result[0]),
                        count: result[1]
                    });
                })
                .catch(next);
        };
    },

    getListOne: function(Model) {
        return function(req, res, next) {
            Model.findById(req.params.id)
                .then(sendJson(res))
                .catch(next);
        }
    },

    getCreateOne: function(Model) {
        return function(req, res, next) {
            delete req.body[Model.getKeyField()];
            new Model(req.body)
                .save()
                .then(sendJson(res))
                .catch(next);
        }
    },

    getUpdateOne: function(Model) {
        return function(req, res, next) {
            delete req.body[Model.getKeyField()];
            Model.findById(req.params.id)
                .then(function (model) {
                    return model
                        .setValues(req.body)
                        .save();
                })
                .then(sendJson(res))
                .catch(next);
        }
    },

    getDeleteOne: function (Model) {
        return function (req, res, next) {
            delete req.body[Model.getKeyField()];
            Model.findById(req.params.id)
                .then(function (model) {
                    return model.remove();
                })
                .then(sendJson(res))
                .catch(next);
        }
    }
};