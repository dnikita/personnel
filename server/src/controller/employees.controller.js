/**
 * Created by ������ on 26.09.2015.
 */

var factory = require('./crud.controller.factory');
var Employees = require('../dao/orm').models.Employees;
var UnprocessableEntity = require('../error/types/UnprocessableEntity');

module.exports = {
    createOne: factory.getCreateOne(Employees),
    updateOne: factory.getUpdateOne(Employees),
    updateList: function(req, res, next) {
        var q = factory.parseQuery(req.query);
        if (Object.keys(q.whereClause).length === 0) {
            return next(new UnprocessableEntity('no condition specified'));
        }
        Employees.batchUpdate(q.whereClause, JSON.parse(req.query.values))
            .then(factory.sendJson(res))
            .catch(next);
    },
    deleteOne: factory.getDeleteOne(Employees),
    deleteList: function(req, res, next) {
        var q = factory.parseQuery(req.query);
        if (Object.keys(q.whereClause).length === 0) {
            return next(new UnprocessableEntity('no condition specified'));
        }
        Employees.batchRemove(q.whereClause)
            .then(factory.sendJson(res))
            .catch(next);
    },
    getOne: factory.getListOne(Employees),
    listAll: factory.getListAll(Employees),
    getPositions: function (req, res, next) {
        var q = factory.parseQuery(req.query);

        Employees.find(q.whereClause, {
            raw: true,
            offset: req.query.offset,
            limit: req.query.limit,
            order: q.orderClause,
            fields: q.fieldsClause
        })
            .then(factory.sendJson(res))
            .catch(next);
    }
};