/**
 * Created by ������ on 26.09.2015.
 */
var express = require('express');
var bodyParser = require('body-parser');
var errorHandler = require('../error/error.middleware');

module.exports = function(app) {
    app.use(express.static(__dirname + '/../../../static'));
    require('../models');
    app.use(bodyParser.json());
    require('../routers')(app);
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');
    app.set('views', __dirname + '/../views');
    app.use(errorHandler);
};