/**
 * Created by ������ on 26.09.2015.
 */

var GenericError = require('../error/types/GenericInternalError');
var NotFoundError = require('../error/types/NotFound');
var Cache = require('../cache/Cache');
var winston = require('winston');

function nullify(el) {
    return typeof el === 'undefined' || el === null ? 'NULL' : el
}

function stringify(el) {
    return typeof el === 'string' ? '"' + el + '"' : el;
}

function isResettingKey(isKeyField, currentId) {
    return isKeyField === true && !(currentId === undefined || currentId === null);
}

module.exports = function(db) {
    var cache = new Cache();

    var SQLModelPrototype = {
        values: null,
        setValues: function(values) {
            var key = this.values && this.values[this.constructor.key];
            this.values = this.constructor._fields.map(function(field) {
                if (typeof values[field.name] !== 'undefined' && !isResettingKey(field.key, key)) {
                    var value = field.type(values[field.name]);
                    if (typeof field.validate === 'function'
                        && field.validate(value) === false) {
                        throw new GenericError('the value ' + value + ' is invalid for the field ' + field.name);
                    }
                    return value;
                } else {
                    return key;
                }
            });
            return this;
        },

        save: function() {
            var name = this.constructor._name;
            var q =  'INSERT OR REPLACE INTO ' + name + ' VALUES (' + this.values.map(function(v) {
                    return nullify(stringify(v));
                }).join(',') + ')';
            winston.debug(q);
            return new Promise(function (res, rej) {
                db.run(q, function (err, data) {
                    if (err) { return rej(new GenericError(err)); }
                    cache.invalidate();
                    res(data);
                });
            });
        },

        remove: function() {
            var keyField = this.constructor.getKeyField();
            var name = this.constructor._name;
            var q =  'DELETE FROM ' + name + ' WHERE (' + keyField+ ' = '+ this.values[this.constructor.key]  + ')';
            winston.debug(q);
            return new Promise(function (res, rej) {
                db.run(q, function (err, data) {
                    if (err) { return rej(new GenericError(err)); }
                    cache.invalidate();
                    res(data);
                });
            });
        },

        toJson: function() {
            var that = this;
            return this.constructor._fields.reduce(function(acc, field, index) {
                acc[field.name] = field.type(that.values[index]);
                return acc;
            }, {});
        }
    };

    function getKeyField() {
        return this._fields[this.key].name;
    }

    function parseConds(where) {
        return Object.keys(where).map(function(key) {
            var val = where[key];
            return Object.keys(val).map(function(cond) {
                return '(' +key + {
                        '$lt' : ' < ',
                        '$gt' : ' > ',
                        '$eq' : ' = ',
                        '$like' : ' LIKE '
                    }[cond] + stringify(val[cond]) + ')';
            }).join(' AND ');
        }).join(' AND ');
    }

    function parseOrder(orders) {
        return orders.map(function(col) {
            var name = Object.keys(col)[0];
            return name + (col[name] < 0 ? ' DESC' : ' ASC');
        }).join(', ');
    }

    function parseAggregations(aggregations) {
        return aggregations instanceof Array
            ? aggregations.join(',')
            : aggregations || '';
    }

    function parseUpdateValues(values) {
        return values.map(function(val) {
            return val.key + '=' + stringify(val.value);
        }).join(', ');
    }

    var batchRemove = function(query) {
        var q = 'DELETE FROM ' + this._name + ' WHERE ' + parseConds(query);
        winston.debug(q);
        return new Promise(function (res, rej) {
            db.run(q, function (err, data) {
                if (err) { return rej(new GenericError(err)); }
                cache.invalidate();
                res(data);
            });
        });
    };

    var batchUpdate = function(query, values) {
        var q = 'UPDATE ' + this._name + ' SET '+ parseUpdateValues(values) + ' WHERE ' + parseConds(query);
        winston.debug(q);
        return new Promise(function (res, rej) {
            db.run(q, function (err, data) {
                if (err) { return rej(new GenericError(err)); }
                cache.invalidate();
                res(data);
            });
        });
    };

    var count = function(query) {
        var cond = parseConds(query);
        var conds = cond ? ' WHERE '  + cond : '';
        var q = 'SELECT COUNT(' + this.getKeyField() + ') as __count FROM ' + this._name + conds ;
        winston.debug(q);
        return new Promise(function (res, rej) {
            var result = cache.get(q);
            if (typeof result === 'undefined') {
                db.all(q, function (err, data) {
                    if (err) { return rej(new GenericError(err)); }
                    var count = null;
                    if (data && data[0].__count) {
                        count = data[0].__count;
                    }
                    res(count);
                    cache.put(q, count);
                });
            } else {
                res(result);
            }
        });
    };

    var find = function(query, opts) {
        'use strict';
        var that = this,
            where = '',
            order = '',
            fields = '*',
            pagination = '',
            limit = 100,
            raw = false,
            offset = 0;

        if (opts) {
            limit = opts.limit | 0 || 100;
            offset = opts.offset | 0;
            raw = opts.raw || false;
            if (opts.order) {
                var orders = parseOrder(opts.order);
                if (orders) {
                    order += ' ORDER BY ' + orders;
                }
            }
            if (opts.fields) {
                fields = opts.fields.map(function(f) {
                    var s = f.unique ? 'DISTINCT ' : '';
                    return s + f.name;
                }).join(',') || '*';
            }
            if (opts.count) {
                fields = 'COUNT('+fields+')';
            }
        }
        if (limit > 0) {
            pagination = ' LIMIT ' + offset + ', ' + limit;
        }

        if (query) {
            var conds = parseConds(query);
            if (conds) {
                where += ' WHERE ' + conds;
            }
        }

        return new Promise(function(ful, rej) {
            var q = 'SELECT '+fields+' FROM ' + that._name + where + order + pagination;
            winston.debug(q);
            var result = cache.get(q);
            if (typeof result === 'undefined') {
                db.all(q, function(err, data) {
                    if (err) return rej(new GenericError(err));
                    cache.put(q, data);
                    ful(raw? data : data.map(that));
                });
            } else {
                winston.debug('loaded from cache');
                ful(raw? result : result.map(that));
            }
        });
    };

    var findAggregated = function (opts) {
        var that = this,
            limit = 100,
            offset = 0,
            aggregate = '',
            count = '';

        if (opts.aggregate) {
            var aggregations = parseAggregations(opts.aggregate);
            if (aggregations) {
                aggregate += ' GROUP BY ' + aggregations;
            }
        }

        if (opts.count) {
            count = ' count(*) AS __count';
        }

        var fields = count
            ? [aggregations, count].join(',')
            : '*';

        var q = 'SELECT ' + fields + ' FROM ' + that._name +  aggregate;

        if (opts.populate) {
            var name = opts.populate.model._name;
            var field = opts.populate.field;
            //todo: this can be generalized for multiple tables
            //and it has to be reorganized
            q = 'SELECT t.*, __count FROM ' + name + ' AS t LEFT OUTER JOIN (' + q + ') AS t2 ON t.' + field + '= t2.' + field;
        }
        q += ' LIMIT ' + offset + ', ' + limit;

        winston.debug(q);
        return new Promise(function (res, rej) {
            var result = cache.get(q);
            if (typeof result === 'undefined') {
                db.all(q, function (err, data) {
                    if (err) return rej(new GenericError(err));
                    cache.put(q, data);
                    res(data);
                });
            } else {
                res(result);
            }
        });
    };

    var findOne = function(q, opts) {
        var that = this;
        return new Promise(function(res, rej) {
            that.find(q, opts).then(function(data) {
                if (data.length) {
                    res(data[0]);
                } else {
                    rej(new NotFoundError());
                }
            }, rej);
        });
    };

    var findById = function(id) {
        var q = {};
        q[this.getKeyField()] = { $eq: +id };
        return this.findOne(q);
    };

    return function(name, fields) {
        function SQLModel(values) {
            if (!(this instanceof SQLModel)) {
                return new SQLModel(values);
            }
            this.setValues(values);
        }
        SQLModel._name = name;
        SQLModel._fields = fields;
        SQLModel.prototype = Object.create(SQLModelPrototype);
        SQLModel.key = fields.reduce(function(key, item, index) {
            if (item.key === true) {
                if (key === null) {
                    key = index;
                } else {
                    throw new GenericError('duplicate key field: ' + key + ', '+ index);
                }
            }
            return key;
        }, null);
        SQLModel.getKeyField = getKeyField;
        SQLModel.prototype.constructor = SQLModel;
        SQLModel.count = count;
        SQLModel.batchRemove = batchRemove;
        SQLModel.batchUpdate = batchUpdate;
        SQLModel.find = find;
        SQLModel.findOne = findOne;
        SQLModel.findById = findById;
        SQLModel.findAggregated = findAggregated;
        SQLModel._parseConds = parseConds;
        return SQLModel;
    };
};