/**
 * Created by ������ on 26.09.2015.
 */
var sqlite3 = require('sqlite3').verbose();
var getSchemaBuilder = require('./SQLiteSchema');

module.exports = {
    connect: function(url) {
        this.defineSchema = getSchemaBuilder(new sqlite3.Database(url));
    },
    define: function(name, fields) {
        return this.models[name] = this.defineSchema(name, fields);
    },
    defineSchema: null,
    models: {}
};