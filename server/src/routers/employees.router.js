/**
 * Created by ndyumin on 28.09.2015.
 */

var empController = require('../controller/employees.controller');
module.exports = function(app) {
    app.post('/api/employees', empController.createOne);
    app.get('/api/employees', empController.listAll);
    app.delete('/api/employees', empController.deleteList);
    app.put('/api/employees/', empController.updateList);
    app.get('/api/employees/positions', empController.getPositions);
    app.get('/api/employees/:id(\\d+)', empController.getOne);
    app.put('/api/employees/:id(\\d+)', empController.updateOne);
    app.delete('/api/departments/:id(\\d+)', empController.deleteOne);
};
