/**
 * Created by ndyumin on 28.09.2015.
 */

var depController = require('../controller/departments.controller');
module.exports = function(app) {
    app.post('/api/departments', depController.createOne);
    app.get('/api/departments', depController.listAll);
    app.get('/api/departments/:id(\\d+)', depController.getOne);
    app.get('/api/departments/emplCount', depController.getDepartmentsWithEmployeeCount);
    app.put('/api/departments/:id(\\d+)', depController.updateOne);
    app.delete('/api/departments/:id(\\d+)', depController.deleteOne);
};