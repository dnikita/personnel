/**
 * Created by ndyumin on 28.09.2015.
 */

module.exports = function initRoutes(app) {
    require('./departments.router')(app);
    require('./employees.router')(app);
    app.use('/:any', function (req, res) {
        res.status(404);
        res.render('404');
    });
};