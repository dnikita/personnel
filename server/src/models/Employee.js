/**
 * Created by ������ on 26.09.2015.
 */

var orm = require('../dao/orm');

module.exports = orm.define('Employees', [
    {name: 'EmplID', type: Number, key: true},
    {name: 'DeptID', type: Number},
    {name: 'FirstName', type: String},
    {name: 'LastName', type: String},
    {name: 'Position', type: String}
]);