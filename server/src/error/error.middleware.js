/**
 * Created by ������ on 29.09.2015.
 */

var PersonnelError = require('./types/PersonnelError');

module.exports = function(err, req, res, next) {
    if (err instanceof PersonnelError) {
        res.status(err.getStatus());
        res.send({
            code: err.getStatus(),
            message : err.getMessage(),
            details : err.getDetails()
        });
    } else if (err instanceof Error) {
        res.status(500);
        res.send({
            code: 500,
            message : err.message,
            details : err.trace
        });
    } else {
        next();
    }
};