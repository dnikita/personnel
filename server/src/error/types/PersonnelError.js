/**
 * Created by ������ on 29.09.2015.
 */

function PersonnelError(details) {
    this.details = details;
}

PersonnelError.prototype.getMessage = function() {
    return this.message || '';
};

PersonnelError.prototype.getDetails = function() {
    return this.details || '';
};

PersonnelError.prototype.getStatus = function() {
    return this.status || 500;
};

module.exports = PersonnelError;