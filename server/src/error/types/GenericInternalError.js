/**
 * Created by ������ on 29.09.2015.
 */

var PersonnelError = require('./PersonnelError');

function InternalError() {
    PersonnelError.apply(this, arguments);
}
InternalError.prototype = new PersonnelError();
InternalError.prototype.message = 'generic error';
InternalError.status = 500;

module.exports = InternalError;