/**
 * Created by Никита on 03.10.2015.
 */

var PersonnelError = require('./PersonnelError');

function Conflict() {
    PersonnelError.apply(this, arguments);
}
Conflict.prototype = new PersonnelError();
Conflict.prototype.message = 'cannot perform in the current state';
Conflict.prototype.status = 409;

module.exports = Conflict;