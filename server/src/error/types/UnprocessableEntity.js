/**
 * Created by ������ on 29.09.2015.
 */

var PersonnelError = require('./PersonnelError');

function UnprocessableEntity() {
    PersonnelError.apply(this, arguments);
}
UnprocessableEntity.prototype = new PersonnelError();
UnprocessableEntity.prototype.message = '';
UnprocessableEntity.prototype.status = 422;

module.exports = UnprocessableEntity;