/**
 * Created by ������ on 29.09.2015.
 */

var PersonnelError = require('./PersonnelError');

function BadRequest() {
    PersonnelError.apply(this, arguments);
}
BadRequest.prototype = new PersonnelError();
BadRequest.prototype.message = 'malformed request';
BadRequest.prototype.status = 400;

module.exports = BadRequest;