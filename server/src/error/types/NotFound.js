/**
 * Created by ������ on 29.09.2015.
 */

var PersonnelError = require('./PersonnelError');

function NotFound() {
    PersonnelError.apply(this, arguments);
}
NotFound.prototype = new PersonnelError();
NotFound.prototype.message = 'resource not found';
NotFound.prototype.status = 404;

module.exports = NotFound;