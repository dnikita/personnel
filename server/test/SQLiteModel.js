var expect = require('chai').expect;
var getSchemaBuilder = require('../src/dao/SQLiteSchema');

describe('SQLite model', function() {
    it('parses values to the specified type', function() {
        var db = {};
        var getSchema = getSchemaBuilder(db);
        var Schema = getSchema('name', [
            {name: 'f1', type: Number},
            {name: 'f2', type: String}
        ]);
        var model = new Schema({
            f1: '1',
            f2: true
        });

        var json = model.toJson();

        expect(json.f1).to.equal(1);
        expect(json.f2).to.equal('true');
    });

    it('validates - positive', function() {
        var db = {};
        var getSchema = getSchemaBuilder(db);
        var Schema = getSchema('name', [
            {
                name: 'f1',
                type: Number,
                validate: function(v) {return v < 20;}
            },
            {
                name: 'f2',
                type: String,
                validate: function(v) {return /^\w{6}$/.test(v)}
            }
        ]);
        var model = new Schema({
            f1: 12,
            f2: 'aabbcc'
        });

        var json = model.toJson();

        expect(json.f1).to.equal(12);
        expect(json.f2).to.equal('aabbcc');
    });
    it('validates - negative', function() {
        var db = {};
        var getSchema = getSchemaBuilder(db);
        var Schema = getSchema('name', [
            {
                name: 'f1',
                type: Number,
                validate: function(v) {return v < 20;}
            },
            {
                name: 'f2',
                type: String,
                validate: function(v) {return /^\w{6}$/.test(v)}
            }
        ]);
        var fail = false;
        try {
            new Schema({
                f1: 112
            });
        } catch (e) {
            fail = true;
        }
        expect(fail).to.be.true;
        fail = false;
        try {
            new Schema({
                f2: 'aaabbcc'
            });
        } catch (e) {
            fail = true;
        }
        expect(fail).to.be.true;
    });
    it('gets records from db with the static `find` method', function(done) {
        this.timeout(1000);
        var db = {
            all: function(q, clb) {
                clb(null, [
                    {f1:1, f2:'a'},
                    {f1:2, f2:'b'},
                    {f1:3, f2:'c'},
                    {f1:4, f2:'d'}
                ]);
            }
        };
        var getSchema = getSchemaBuilder(db);
        var Schema = getSchema('name', [
            {name: 'f1', type: Number},
            {name: 'f2', type: String}
        ]);
        Schema.find({}).then(function(data) {
            var jsonData = data.map(function(a) {return a.toJson()});
            expect(jsonData[2].f1).to.equal(3);
            expect(jsonData[3].f2).to.equal('d');
            done();
        });
    });

    it('gets a record from db with the static `findOne` method', function(done) {
        this.timeout(2000);
        var db = {
            all: function(q, clb) {
                clb(null, [
                    {f1:1, f2:'a'},
                    {f1:2, f2:'b'},
                    {f1:3, f2:'c'},
                    {f1:4, f2:'d'}
                ]);
            }
        };
        var getSchema = getSchemaBuilder(db);
        var Schema = getSchema('name', [
            {name: 'f1', type: Number},
            {name: 'f2', type: String}
        ]);
        Schema.findOne({}).then(function(data) {
            expect(data.toJson().f2).to.equal('a');
            done();
        });
    });

    xit('gets a record from db with the static `findOne` method', function(done) {
        this.timeout(2000);
        var db = {
            all: function(q, clb) {
                clb(null, []);
            }
        };
        var getSchema = getSchemaBuilder(db);
        var Schema = getSchema('name', [
            {name: 'f1', type: Number},
            {name: 'f2', type: String}
        ]);
        Schema.findOne({}).then(function(data) {
            expect(data).to.be.null;
            done();
        });
    });

    xit('saves records to db', function(done) {
        this.timeout(1000);
        var db = {
            run: function(q, clb) {
                clb(null, q);
            }
        };
        var getSchema = getSchemaBuilder(db);
        var Schema = getSchema('name', [
            {name: 'f1', type: Number},
            {name: 'f2', type: String}
        ]);
        var model = new Schema({
            f1: 1,
            f2: 'abc'
        });
        model.save().then(function(data) {
            expect(data).to.equal('INSERT OR REPLACE INTO name VALUES (1,abc)');
            done();
        });
    });

    it('allows basic filtering by field values', function() {
        this.timeout(1000);
        var db = {
            run: function(q, clb) {
                clb(null, q);
            }
        };
        var getSchema = getSchemaBuilder(db);
        var Schema = getSchema('name', [
            {name: 'f1', type: Number},
            {name: 'f2', type: String}
        ]);

        var where = Schema._parseConds({
            'f1': {
                $gt: 12,
                $lt: 24
            },
            'f2': {
                $like: '"a%"'
            }
        });
        expect(where).to.equal('(f1 > 12) AND (f1 < 24) AND (f2 LIKE "\"a%\"")');
    });
});
