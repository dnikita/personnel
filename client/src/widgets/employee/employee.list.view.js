/**
 * Created by ndyumin on 01.10.2015.
 */

var ListView = require('../../base/mvc/ListView');

function EmployeeListView() {
    ListView.apply(this, arguments)
}

EmployeeListView.prototype = Object.create(ListView.prototype);

EmployeeListView.prototype.template = [
    '<table class="table table-striped">',
    '<thead>',
    '<td>Имя</td>',
    '<td>Фамилия</td>',
    '<td>Должность</td>',
    '</thead>',
    '<tbody>{{list}}</tbody>',
    '</table>'
].join('');

EmployeeListView.prototype.rowTemplate = [
    '<tr data-id="{{EmplID}}">',
    '<td>{{FirstName}}</td>',
    '<td>{{LastName}}</td>',
    '<td>{{Position}}</td>',
    '<tr>'
].join('');

EmployeeListView.prototype.parseItem = function(d) {
    if (d.Position === "-1") {
        d.Position = '';
    }
    return d;
};
module.exports = EmployeeListView;