/**
 * Created by ndyumin on 01.10.2015.
 */

var Model = require('../../base/mvc/ListModel');

function EmployeeListMorning() {
    Model.apply(this, arguments);
}

EmployeeListMorning.prototype = Object.create(Model.prototype);
EmployeeListMorning.prototype.getRequestUrl = function(id) {
    return 'api/employees/?where={"DeptID":{"$eq":' + id + '}}';
};

EmployeeListMorning.prototype.parse = function(d) {
    this.count = d.count;
    return d.data;
};

module.exports = EmployeeListMorning;