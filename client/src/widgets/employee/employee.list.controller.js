/**
 * Created by ndyumin on 01.10.2015.
 */
var EmployeesView = require('./employee.list.view');
var EmployeesModel = require('./employee.list.model');
var Controller = require('../../base/mvc/Controller');

function EmployeeListController(root, app) {
    Controller.apply(this, arguments);
    this.model = new EmployeesModel();
    this.view = new EmployeesView(root, this.model);
}

EmployeeListController.prototype = Object.create(Controller.prototype);
EmployeeListController.prototype.dispatch = function (msg) {
    if (typeof msg.id !== 'undefined') {
        this.model.load(msg.id)
            .catch(this.handleAjaxError);
    }
};
EmployeeListController.prototype.dispose = function () {
    this.model.dispose();
    this.view.dispose();
    this.view = this.model = null;
    Controller.prototype.dispose.call(this);
};

module.exports = EmployeeListController;