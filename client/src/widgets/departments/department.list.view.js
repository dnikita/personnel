/**
 * Created by ndyumin on 30.09.2015.
 */
var ListView = require('../../base/mvc/ListView');

function DepartmentListView() {
    ListView.apply(this, arguments)
}

DepartmentListView.prototype = Object.create(ListView.prototype);
DepartmentListView.prototype.template = [
    '<table class="table table-striped">',
        '<thead>',
            '<td>Название</td>',
            '<td>Сотрудники</td>',
            '<td></td>',
        '</thead>',
        '<tbody>{{list}}</tbody>',
    '</table>'
].join('');
DepartmentListView.prototype.rowTemplate = [
    '<tr data-id="{{DeptID}}">',
        '<td>{{DeptName}}</td>',
        '<td>{{__count}}</td>',
        '<td data-action="remove">X</td>',
    '<tr>'
].join('');
DepartmentListView.prototype.parseItem = function(d) {
    d.__count = d.__count ? d.__count : '';
    return d;
};
module.exports = DepartmentListView;