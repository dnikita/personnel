/**
 * Created by ndyumin on 30.09.2015.
 */

var Model = require('./department.list.model.js');
var View = require('./department.list.view.js');
var Controller = require('../../base/mvc/Controller');

function DepartmentListController(root, app) {
    Controller.apply(this, arguments);
    this.model = new Model();
    this.view = new View(root, this.model);
    this.model.load().catch(this.handleAjaxError);
    this._onClick = this._onClick.bind(this);
    this.$root.addEventListener('click', this._onClick);
}

DepartmentListController.prototype = Object.create(Controller.prototype);

DepartmentListController.prototype.removeDepartment = function(id) {
    var model = this.model;
    var bus = this.app.eventBus;

    model.remove(id)
        .then(function () {
            bus.trigger('message', {
                code: 200,
                message: 'подразделение удалено'
            });
            model.load();
        })
        .catch(this.handleAjaxError);
};

DepartmentListController.prototype._handleCellClick = function(e) {
    var id = e.target.parentNode.dataset.id;
    if (typeof  id !== 'undefined') {
        switch (e.target.dataset.action) {
            case 'remove':
                this.removeDepartment(id);
                break;
            default:
                this.app.eventBus.trigger('form', {
                    base: 'department',
                    message: {id: id}
                });
                break;
        }
    }
};

DepartmentListController.prototype._onClick = function (e) {
    if (e.target.tagName === 'TD') {
        this._handleCellClick(e);
    }
};

DepartmentListController.prototype.dispose = function () {
    this.$root.removeEventListener('click', this._onClick);
    this.model.dispose();
    this.view.dispose();
    Controller.prototype.dispose.call(this);
};

module.exports = DepartmentListController;