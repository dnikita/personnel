/**
 * Created by ndyumin on 30.09.2015.
 */

var Model = require('../../base/mvc/ListModel');
var Util = require('../../base/Util');

function DepartmentListModel() {
    Model.call(this, 'api/departments/emplCount');
}

DepartmentListModel.prototype = Object.create(Model.prototype);

DepartmentListModel.prototype.getRequestUrl = function(id, method) {
    return typeof id !== 'undefined'
        ? 'api/departments/' + id
        : 'api/departments/emplCount';
};

DepartmentListModel.prototype.load = function() {
    var that = this;
    return Util.ajax('api/departments').then(function(data){
        that.trigger('change', JSON.parse(data).data);
        Model.prototype.load.call(that).catch(that.handleAjaxError);
    });
};

module.exports = DepartmentListModel;