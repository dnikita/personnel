/**
 * Created by Никита on 04.10.2015.
 */
var View = require('../../base/mvc/ListView');

function PaginationView(root, model) {
    View.apply(this, arguments);
}

PaginationView.prototype = Object.create(View.prototype);

PaginationView.prototype.template = [
    '<ul class="pagination">{{list}}</ul>'
].join('');

PaginationView.prototype.rowTemplate = [
    '<li class="{{active}}"><a href="#" data-index="{{index}}">{{label}}</a></li>'
].join('');

module.exports = PaginationView;