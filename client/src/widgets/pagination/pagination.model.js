/**
 * Created by Никита on 04.10.2015.
 */
var EventEmitter = require('../../base/EventEmitter');

function PaginationModel() {
    EventEmitter.apply(this, arguments);
    this.page = 0;
    this.size = 10;
    this.total = 30;
}

PaginationModel.prototype = Object.create(EventEmitter.prototype);

PaginationModel.prototype._fireUpdate = function () {
    var data = [];
    var max = Math.ceil(this.total / this.size);
    var i;

    function push(i, active) {
        data.push({
            index : i,
            label: i + 1,
            active: active ? 'active' : ''
        })
    }

    if (max > 7) {
        push(0, 0 === this.page);
        var start = Math.max(this.page - 3, 1);
        var end;

        if (start + 5 > max - 2) {
            end = max - 2;
            start = end - 5;
        } else {
            end = start + 5;
        }
        for (i = start; i < end; i++) {
            push(i, i === this.page);
        }
        push(max - 1, max - 1 === this.page);
    } else if (max > 1){
        for (i = 0; i < max; i += 1) {
            push(i, i === this.page);
        }
    }

    this.trigger('change', data);
};

PaginationModel.prototype._firePageChange = function () {
    this.trigger('pageChange', this.page);
};
PaginationModel.prototype.setSize = function (d) {
    if (d !== this.size) {
        this.size = d;
        var page = this.page;
        this.page = 0;
        this._fireUpdate();
        if (page !== this.page) {
            this._firePageChange();
        }
    }
};

PaginationModel.prototype.setTotal = function (d) {
    if (d !== this.total) {
        this.total = d;
        var page = this.page;
        this.page = 0;
        this._fireUpdate();
        if (page !== this.page) {
            this._firePageChange();
        }
    }
};

PaginationModel.prototype.setPage = function (d) {
    var max = Math.ceil(this.total / this.size);
    if (d >= 0 && d < max && this.page !== d) {
        this.page = d;
        this._fireUpdate();
        this._firePageChange();
    }
};

PaginationModel.prototype.prev = function () {
    this.page--;
    if (this.page < 0) {
        this.page = 0;
    } else {
        this._fireUpdate();
        this._firePageChange();
    }
};

PaginationModel.prototype.next = function () {
    this.page++;
    var max = Math.floor(this.total / this.size);
    if (this.page > max) {
        this.page = max;
    } else {
        this._fireUpdate();
        this._firePageChange();
    }
};

PaginationModel.prototype.dispose = function() {
    this.off();
};

module.exports = PaginationModel;