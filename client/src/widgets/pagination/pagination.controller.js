/**
 * Created by Никита on 04.10.2015.
 */

var Model = require('./pagination.model');
var View = require('./pagination.view');

function PaginationController(root, app) {
    this.root = root;
    this.$root = document.querySelector(root);
    this._onClick = this._onClick.bind(this);
    this.$root.addEventListener('click', this._onClick);

    this.model = new Model();
    this.view = new View(root, this.model);
}

PaginationController.prototype = {
    _onClick: function(e) {
        if (typeof e.target.dataset.index !== 'undefined') {
            this.model.setPage(+e.target.dataset.index);
        }
    },
    redelegate: function() {
        this.$root.removeEventListener('click', this._onClick);
        this.$root = document.querySelector(this.root);
        this.$root.addEventListener('click', this._onClick);
    },
    dispose: function() {
        this.model.dispose();
        this.view.dispose();
        this.$root.removeEventListener('click', this._onClick);
        this.$root = null;
    }
};

module.exports = PaginationController;