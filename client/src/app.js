/**
 * Created by ndyumin on 25.09.2015.
 */
var EventEmitter = require('./base/EventEmitter');

var Main = require('./forms/main/main.controller');
var Personnel = require('./forms/personnel/personnel.controller');
var DepItemController = require('./forms/departmentView/department.item.controller.js');
var MessageController = require('./message/messages.controller');
var APP_ROOT = '#appRoot';

function Application(eventBus) {
    this.state = null;
    this.form = null;
    this.eventBus = eventBus;
    this.messages = new MessageController('body', this);

    this.eventBus.on('form', this._onFormChange.bind(this));
    this.eventBus.trigger('form', {
        base: 'main'
    });
}

Application.prototype = Object.create({
    _onFormChange: function (state) {
        if ((this.state && this.state.base) !== state.base) {
            if (this.form) {
                this.form.dispose();
            }
            switch (state.base) {
                case 'main':
                    this.form = new Main(APP_ROOT, this);
                    break;
                case 'department':
                    this.form = new DepItemController(APP_ROOT, this);
                    break;
                case 'personnel':
                    this.form = new Personnel(APP_ROOT, this);
                    break;
                default:
                    throw new Error('unknown form');
                    break;
            }
        }
        this.form.dispatch(state.message);
        this.state = state;
    }
});

new Application(new EventEmitter());


