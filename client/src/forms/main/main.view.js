/**
 * Created by ndyumin on 30.09.2015.
 */
var View = require('../../base/mvc/View');

function DepartmentListView() {
    View.apply(this, arguments)
}

DepartmentListView.prototype = Object.create(View.prototype);
DepartmentListView.prototype.template = [
    '<h1>Подразделения</h1>',
    '<div data-action="toPersonnel" class="btn btn-default">Управление персоналом</div>',
    '<form class="form-horizontal">',
        '<div class="title">Новое подразделение</div>',
        '<div class="form-group">',
            '<label for="newDepartmentName" class="col-sm-2 control-label">Название</label>',
            '<div class="col-sm-10">',
                '<input id="newDepartmentName" type="text"  class="form-control" />',
            '</div>',
        '</div>',
        '<div class="form-group">',
            '<div class="col-sm-offset-2 col-sm-10">',
                '<button data-action="addDepartment" type="button" class="btn btn-default" >добавить</button>',
            '</div>',
        '</div>',

    '</form>',
    '<div class="depList"></div>'
].join('');

module.exports = DepartmentListView;