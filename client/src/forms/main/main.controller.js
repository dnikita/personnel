/**
 * Created by ndyumin on 10/2/2015.
 */
var Model = require('../../base/mvc/Model');
var Controller = require('../../base/mvc/Controller');
var View = require('./main.view');
var Util = require('../../base/Util');
var DepListController = require('../../widgets/departments/department.list.controller');

function EmployeeListController(root, app) {
    Controller.apply(this, arguments);

    this.model = new Model();
    this.view = new View(root, this.model);
    this.view.render();
    this.employeesListWidget = new DepListController('.depList', app);
    this._onClick = this._onClick.bind(this);
    this.$root.addEventListener('click', this._onClick)
}

EmployeeListController.prototype = Object.create(Controller.prototype);
EmployeeListController.prototype._addDepartment = function () {
    var that = this;
    var $input = that.$root.querySelector('input#newDepartmentName');
    Util.ajax({
        method: 'POST',
        url: 'api/departments/',
        data: JSON.stringify({
            "DeptName": $input.value
        })
    }).then(function () {
        that.app.eventBus.trigger('message', {
            code: 200,
            message: 'подразделение создано',
            details: $input.value
        });
        $input.value = '';
        that.employeesListWidget.model.load();
    }).catch(this.handleAjaxError);
};

EmployeeListController.prototype._onClick = function (e) {
    switch (e.target.dataset.action) {
        case 'toPersonnel':
            this.app.eventBus.trigger('form', {
                base: 'personnel'
            });
            break;
        case 'addDepartment':
            this._addDepartment();
            break;
    }
};

EmployeeListController.prototype.dispose = function () {
    this.$root.removeEventListener('click', this._onClick);
    this.employeesListWidget.dispose();
    this.model.dispose();
    this.view.dispose();
    this.employeesListWidget = null;
    this.view = this.model = null;
    Controller.prototype.dispose.call(this);
};

module.exports = EmployeeListController;

