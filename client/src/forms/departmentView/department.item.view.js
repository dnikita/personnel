/**
 * Created by ndyumin on 01.10.2015.
 */
var View = require('../../base/mvc/View');

function DepartmentItemView() {
    View.apply(this, arguments);
}

DepartmentItemView.prototype = Object.create(View.prototype);
DepartmentItemView.prototype.template = [
    '<h1>{{DeptName}}</h1>',
    '<div data-action="back" class="btn btn-default">Назад</div>',
    '<div class="employeeList"></div>',
    '<div class="pagination"></div>'
].join('');

module.exports = DepartmentItemView;