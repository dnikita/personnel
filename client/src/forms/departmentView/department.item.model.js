/**
 * Created by ndyumin on 01.10.2015.
 */

var ItemModel = require('../../base/mvc/Model');

function DepartmentItemModel() {
    ItemModel.call(this, 'api/departments/');
}

DepartmentItemModel.prototype = Object.create(ItemModel.prototype);

module.exports = DepartmentItemModel;