/**
 * Created by ndyumin on 01.10.2015.
 */

var EmployeesController = require('../../widgets/employee/employee.list.controller');
var PaginationController = require('../../widgets/pagination/pagination.controller');
var DepartmentItemView = require('./department.item.view');
var DepartmentItemModel = require('./department.item.model');
var Controller = require('../../base/mvc/Controller');

function DepartmentItemController(root, app) {
    Controller.apply(this, arguments);

    this._onClick = this._onClick.bind(this);
    this.$root.addEventListener('click', this._onClick);
    var model = this.model = new DepartmentItemModel();
    this.view = new DepartmentItemView(root, this.model);
    this.view.render({});
    var employeesWidget = this.employeesWidget = new EmployeesController('.employeeList', app);
    var paginationWidget = this.paginationWidget = new PaginationController('.pagination', app);

    this.model.on('change', function(data) {
        employeesWidget.dispatch({
            id: data.DeptID
        });
        paginationWidget.redelegate();
    });
    this.paginationWidget.model.on('pageChange', function() {
        employeesWidget.model.load(model.data.DeptID);
    });
    this.employeesWidget.model.getRequestUrl = function (id) {
        var page = paginationWidget.model.page;
        var size = paginationWidget.model.size;
        return 'api/employees/?limit='+size+'&offset='+(size*page)+'&where={"DeptID":{"$eq":' + id + '}}';
    };

    this.employeesWidget.model.on('change', function() {
        var count = employeesWidget.model.count;
        paginationWidget.model.setTotal(count);
    });
}

DepartmentItemController.prototype = Object.create(Controller.prototype);
DepartmentItemController.prototype._onClick = function (e) {
    switch (e.target.dataset.action) {
        case 'back':
            this.app.eventBus.trigger('form', {
                base: 'main'
            });
            break;
    }
};

DepartmentItemController.prototype.dispatch = function (msg) {
    this.model.load(msg.id)
        .catch(this.handleAjaxError);
};

DepartmentItemController.prototype.dispose = function () {
    this.$root.removeEventListener('click', this._onClick);
    this.model.off();
    this.employeesWidget.dispose();
    this.paginationWidget.dispose();
    this.view = this.model = this.employeesWidget = null;
    Controller.prototype.dispose.call(this);
};

module.exports = DepartmentItemController;