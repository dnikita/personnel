/**
 * Created by ndyumin on 10/2/2015.
 */
var Model = require('../../base/mvc/Model');
var Controller = require('../../base/mvc/Controller');
var View = require('./personnel.view');
var Util = require('../../base/Util');
var F = require('../../base/F');
var EmployeeListController = require('../../widgets/employee/employee.list.controller');
var PaginationController = require('../../widgets/pagination/pagination.controller');

function PersonnelFormController(root, app) {
    Controller.apply(this, arguments);

    var that = this;
    this.model = new Model();
    this.view = new View(root, this.model);
    this.view.render();
    this._fillDepartments();
    this._fillPositions();

    this.employeesListWidget = new EmployeeListController('.emplList', app);
    this.paginationWidget = new PaginationController('.pagination', app);

    this.employeesListWidget.model.getRequestUrl = function () {
        var page = that.paginationWidget.model.page;
        var size = that.paginationWidget.model.size;
        return 'api/employees/?limit='+size+'&offset='+(size*page)+'&where=' + JSON.stringify(that.getQuery());
    };

    this.employeesListWidget.model.on('change', function() {
        var count = that.employeesListWidget.model.count;
        that.paginationWidget.model.setTotal(count);
    });

    this.paginationWidget.model.on('pageChange', function() {
        that.employeesListWidget.model.load();
    });
    this.employeesListWidget.model.load();

    this._onClick = this._onClick.bind(this);
    this.$root.addEventListener('click', this._onClick)
}

PersonnelFormController.prototype = Object.create(Controller.prototype);
PersonnelFormController.prototype.getQuery = function () {
    var q = {};
    var dep = this.$root.querySelector('#searchDepartments').value;
    var pos = this.$root.querySelector('#searchPositions').value;
    var fname = this.$root.querySelector('#searchFirstName').value;
    var lname = this.$root.querySelector('#searchLastName').value;

    if (dep !== '') {
        q.DeptID = {
            $eq: dep
        };
    }
    if (pos !== '') {
        q.Position = {
            $eq: pos
        };
    }
    if (fname !== '') {
        q.FirstName = {
            $like: fname
        };
    }
    if (lname !== '') {
        q.LastName = {
            $like: lname
        };
    }
    return q;
};

PersonnelFormController.prototype._addEmployee = function () {
    var that = this;
    var $firstName = that.$root.querySelector('input#newEmployeeFirstName');
    var $lastName = that.$root.querySelector('input#newEmployeeLastName');
    Util.ajax({
        method: 'POST',
        url: 'api/employees/',
        data: JSON.stringify({
            "FirstName": $firstName.value,
            "LastName": $lastName.value,
            "Position": -1,
            "DeptID": -1
        })
    }).then(function () {
        that.app.eventBus.trigger('message', {
            code: 200,
            message: 'сотрудник добавлен',
            details: $firstName.value + ' ' + $lastName.value
        });
        $firstName.value = '';
        $lastName.value = '';

        that.employeesListWidget.model.load();
    }).catch(this.handleAjaxError);
};

PersonnelFormController.prototype._removeEmployees = function() {
    var that = this;
    Util.ajax({
        method: 'DELETE',
        url: 'api/employees/?where=' + JSON.stringify(this.getQuery())
    }).then(function () {
        that.app.eventBus.trigger('message', {
            code: 200,
            message: 'сотрудники уволены'
        });
        that.employeesListWidget.model.load();
    }).catch(this.handleAjaxError);
};

PersonnelFormController.prototype._updateEmployees = function (field) {
    var that = this;
    var query = JSON.stringify(this.getQuery());
    var values = JSON.stringify([{
        key: field,
        value: this.$root.querySelector('[data-field='+field+']').value
    }]);

    Util.ajax({
        method: 'PUT',
        url: 'api/employees/?where=' + query + '&values=' + values
    }).then(function () {
        that.app.eventBus.trigger('message', {
            code: 200,
            message: 'сотрудники переведены'
        });
        that.employeesListWidget.model.load();
    }).catch(this.handleAjaxError);
};

PersonnelFormController.prototype._onClick = function (e) {
    switch (e.target.dataset.action) {
        case 'findEmployees':
            this.employeesListWidget.model.load();
            break;
        case 'removeEmployee':
            this._removeEmployees();
            break;
        case 'moveEmplToDep':
            this._updateEmployees('DeptID');
            break;
        case 'moveEmplToPos':
            this._updateEmployees('Position');
            break;
        case 'back':
            this.app.eventBus.trigger('form', {
                base: 'main'
            });
            break;
        case 'addEmployee':
            this._addEmployee();
            break;
    }
};

PersonnelFormController.prototype.parseDict = function(d) {
    return d.data;
};

PersonnelFormController.prototype.parsePos = function(d) {
    return d.filter(function(d) {
        return d.Position !== '-1';
    });
};

PersonnelFormController.prototype._fillDepartments = function () {
    var that = this;
    Util.ajax('api/departments/')
        .then(F.compose(JSON.parse, that.parseDict, that.view.renderDepartments))
        .catch(this.handleAjaxError);
};

PersonnelFormController.prototype._fillPositions = function () {
    var that = this;
    Util.ajax('api/employees/positions/?fields=[{"name":"Position","unique":1}]&order=[{"Position":1}]')
        .then(F.compose(JSON.parse, that.parsePos, that.view.renderPositions))
        .catch(this.handleAjaxError);
};

PersonnelFormController.prototype.dispose = function () {
    this.$root.removeEventListener('click', this._onClick);
    this.employeesListWidget.dispose();
    this.paginationWidget.dispose();
    this.model.dispose();
    this.view.dispose();
    this.employeesListWidget = null;
    this.view = this.model = null;
    Controller.prototype.dispose.call(this);
};

module.exports = PersonnelFormController;

