/**
 * Created by ndyumin on 30.09.2015.
 */
var View = require('../../base/mvc/View');
var Util = require('../../base/Util');
var template = require('./personnel.template');
function PersonnelFormView() {
    View.apply(this, arguments);
    var that = this;
    this.renderDepartments = function(data) {
        var template = '<option value="{{DeptID}}">{{DeptName}}</option>';
        data.unshift({
            "DeptName": 'в распоряжении',
            "DeptID": -1
        });
        that._renderSelect.call(that, '#moveDepartments', template, data);
        data.unshift({
            "DeptName": '',
            "DeptID": ''
        });
        that._renderSelect.call(that, '#searchDepartments', template, data);
    };
    this.renderPositions = function (data) {
        var template = '<option value="{{key}}">{{Position}}</option>';
        data = data.map(function(item) {
            item.key = item.Position;
            return item;
        });
        data.unshift({
            "key": -1,
            "Position": 'не назначен'
        });
        that._renderSelect.call(that, '#movePositions', template, data);
        data.unshift({
            "key": '',
            "Position": ''
        });
        that._renderSelect.call(that, '#searchPositions', template, data);
    };
}

PersonnelFormView.prototype = Object.create(View.prototype);
PersonnelFormView.prototype.template = template;

PersonnelFormView.prototype._renderSelect = function(selector, template, data) {
    var item = Util.template(template);
    document.querySelector(this.rootSelector)
        .querySelector(selector)
        .innerHTML = data
        .map(function (r) {
            Object.keys(r).forEach(function(field) {
                r[field] = Util.stringify(r[field]);
            });
            return r;
        })
        .map(item).join('');
};

module.exports = PersonnelFormView;