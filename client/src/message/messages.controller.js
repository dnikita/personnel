/**
 * Created by ndyumin on 10/2/2015.
 */

var Util = require('../base/Util');

function MessagesController(rootSelector, app) {
    this.$root = document.querySelector(rootSelector);
    this.$notifRoot = this._createMessagesBlock();
    this.$root.appendChild(this.$notifRoot);
    this._onMessage = this._onMessage.bind(this);
    this.app = app;
    this.app.eventBus.on('message', this._onMessage)
}

MessagesController.prototype = {
    _createMessagesBlock: function() {
        var notificationsRoot = document.createElement('div');
        notificationsRoot.id = 'notifications';
        return notificationsRoot;
    },
    _createMessage: function(opts) {
        var notification = document.createElement('div');
        notification.innerHTML = [opts.message, opts.details].map(Util.empty).join('<br>');
        notification.style.backgroundColor = (opts.code|0) >= 300 ? 'red' : 'green';
        notification.className = 'notification';
        return notification;
    },
    _onMessage: function(ops) {
        var notification = this._createMessage(ops);
        this.$notifRoot.appendChild(notification);
        setTimeout(function () {
            notification.className = 'notification hidden';
            setTimeout(function () {
                notification.parentNode.removeChild(notification);
            }, 5000);
        }, 1000);
    },
    dispatch: function() {},
    dispose: function() {
        this.app.off('message', this._onMessage);
        this.$root = this.app = null;
    }
};


module.exports = MessagesController;