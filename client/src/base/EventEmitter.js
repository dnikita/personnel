/**
 * Created by ndyumin on 30.09.2015.
 */

function EventEmitter() {
    this.subs = {};
}

EventEmitter.prototype = Object.create({
    on: function(name, clb) {
        if (typeof this.subs[name] === 'undefined') {
            this.subs[name] = [];
        }
        this.subs[name].push(clb);
    },
    off: function(name, clb) {
        switch(arguments.length) {
            case 0:
                Object.keys(this.subs).forEach(function(evName) {
                    this.subs[evName] = [];
                }, this);
                break;
            case 1:
                this.subs[name] = [];
                break;
            case 2:
                this.subs[name] = this.subs[name].filter(function(item) {
                    return item !== clb;
                });
                break;
        }
    },
    trigger: function(name, data) {
        if (this.subs[name] instanceof Array) {
            this.subs[name].forEach(function(clb) {
                clb(data);
            });
        }
    }
});

module.exports = EventEmitter;