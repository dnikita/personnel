/**
 * Created by ndyumin on 01.10.2015.
 */

var Util = require('../Util');
var F = require('../F');

function View(rootSelector, model) {
    this.rootSelector = rootSelector;
    this.model = model;
    this.tpl = Util.template(this.template);
    this.model = model;
    this.render = this.render.bind(this);
    this.model.on('change', this.render);
}

View.prototype = {
    template: '',
    parse: F.IDENTITY,
    render: function(data) {
        var el = document.querySelector(this.rootSelector);
        el.innerHTML = this.tpl(this.parse(data));
    },
    dispose: function() {
        this.tpl = this.model = null;
    }
};

module.exports = View;