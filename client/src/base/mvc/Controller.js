/**
 * Created by Никита on 03.10.2015.
 */
function Controller(root, app) {
    this.app = app;
    this.$root = document.querySelector(root);
    this.handleAjaxError = this.handleAjaxError.bind(this);
}

Controller.prototype = {
    handleAjaxError: function (e) {
        this.app.eventBus.trigger('message', JSON.parse(e));
    },
    dispatch: function() {},
    dispose: function() {
        this.app = this.$root = null;
    }
};

module.exports = Controller;