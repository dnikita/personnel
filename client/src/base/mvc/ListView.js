/**
 * Created by ndyumin on 01.10.2015.
 */

var View = require('./View');
var Util = require('../Util');
var F = require('../F');

function ListView() {
    View.apply(this, arguments);
    this.rowTpl = Util.template(this.rowTemplate);
}

ListView.prototype = Object.create(View.prototype);
ListView.prototype.rowTemplate = '';
ListView.prototype.parseItem =  F.IDENTITY;
ListView.prototype.render = function (data) {
    View.prototype.render.call(this, {
        list: data.map(F.compose(this.parseItem, this.rowTpl)).join('')
    });
};

module.exports = ListView;