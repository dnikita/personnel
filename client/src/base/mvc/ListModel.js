/**
 * Created by ndyumin on 01.10.2015.
 */
var Model = require('./Model');

function ListModel() {
    Model.apply(this, arguments);
}

ListModel.prototype = Object.create(Model.prototype);

ListModel.prototype.getRequestUrl = function(data) {
    return this.url;
};

module.exports = ListModel;