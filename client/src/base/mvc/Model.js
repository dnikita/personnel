/**
 * Created by ndyumin on 01.10.2015.
 */
var EventEmitter = require('../EventEmitter');
var Util = require('../Util');
var F = require('../F');

function ItemModel(url) {
    EventEmitter.apply(this, arguments);
    this.url = url;
}

ItemModel.prototype = Object.create(EventEmitter.prototype);
ItemModel.prototype.getRequestUrl = function(data) {
    return this.url + data;
};

ItemModel.prototype.load = function (data) {
    var that = this;
    this.state = data;
    function parse(raw) {
        try {
            that.data = JSON.parse(raw);
            that.trigger('change', that.parse(that.data));
        } catch(e) {
            throw new Error('cannot parse the response');
        }
    }

    return Util.ajax(this.getRequestUrl(data))
        .then(parse);
};

ItemModel.prototype.parse = F.IDENTITY;

ItemModel.prototype.remove = function(id) {
    return Util.ajax({
        method: 'DELETE',
        url: this.getRequestUrl(id, 'DELETE')
    });
};

ItemModel.prototype.dispose = function () {
    this.off();
};

module.exports = ItemModel;