/**
 * Created by ������ on 03.10.2015.
 */

var F = require('./F');

var STATE = {
    PENDING: 0,
    RESOLVED: 1,
    REJECTED: 2
};
/**
 * a simple promise: no composition, 1 resolve and 1 reject handler
 * @param exec
 * @constructor
 */
function SimplePromise(exec) {
    var that = this;

    function res(d) {
        that.data = d;
        that.state = STATE.RESOLVED;
        that._fulfil();
    }

    function rej(d) {
        that.data = d;
        that.state = STATE.REJECTED;
        that._fulfil();
    }

    try {
        exec(res, rej);
    } catch (e) {
        rej(e);
    }
}

SimplePromise.prototype = {
    state: STATE.PENDING,
    _fulfil: function () {
        switch (this.state) {
            case STATE.RESOLVED:
                F.callIfFunction(this._onSuccess, [this.data], this);
                break;
            case STATE.REJECTED:
                F.callIfFunction(this._onFail, [this.data], this);
                break;
        }
    },
    then: function (onSuccess_) {
        this._onSuccess = onSuccess_;
        this._fulfil();
        return this;
    },
    catch: function (onFail_) {
        this._onFail = onFail_;
        this._fulfil();
        return this;
    }
};

module.exports = SimplePromise;
