/**
 * Created by ndyumin on 30.09.2015.
 */

var Promise = window.Promise || require('./SimplePromise');

var loader = null;
function getModalLoader() {
    if (loader !== null) return loader;
    var loader_ = document.createElement('div');
    loader_.className = 'loader';
    loader = loader_;
    document.body.appendChild(loader);
    return loader;
}

module.exports = {
    template: function (tpl) {
        return function (params) {
            return tpl.replace(/{{(\w+)}}/g, function (_, g1) {
                return typeof params[g1] !== 'undefined'
                    ? params[g1]
                    : '';
            })
        };
    },
    ajax: function (url) {
        var method = 'GET';
        var data = null;
        if (url && typeof url === 'object') {
            method = url.method || method;
            data = url.data || data;
            url = url.url;
        }

        return new Promise(function (res, rej) {
            var xhr = new XMLHttpRequest();
            xhr.open(method, encodeURI(url));
            //getModalLoader().classList.add('show');
            xhr.onload = function () {
                if (xhr.status >= 200 && xhr.status < 300) {
                    res(xhr.responseText);
                } else {
                    rej(xhr.responseText);
                }
                //getModalLoader().classList.remove('show');
            };
            xhr.onerror = function () {
                rej(xhr.responseText);
                //getModalLoader().classList.remove('show');
            };
            xhr.setRequestHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
            xhr.setRequestHeader('Pragma', 'no-cache');
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send(data);
        });
    },
    empty: function (s) {
        return typeof s === 'string' ? s : '';
    },
    stringify: function (s) {
        return  s !== null ? s : '';
    }
};