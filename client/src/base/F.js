/**
 * Created by ������ on 03.10.2015.
 */
module.exports = {
    callIfFunction: function (f, args, ctx) {
        return typeof f === 'function'
            ? f.apply(ctx || null, args)
            : undefined;
    },
    IDENTITY: function(d) {return d},
    compose: function() {
        var funcs = Array.prototype.slice.call(arguments);
        return function(inp) {
            return funcs.reduce(function(arg, f) {
                return f.call(null, arg);
            }, inp);
        }
    }
};