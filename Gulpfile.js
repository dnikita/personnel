var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var watchify = require('watchify');
var gulp = require('gulp');
var mocha = require('gulp-mocha');
var uglify = require('gulp-uglify');

function getBundler(watch) {
    return function () {
        var b = browserify('./client/src/app.js', {
            cache: {},
            packageCache: {},
            debug : true,
            fullPaths: true
        });

        if (watch) {
            watchify(b).on('update', build);
        }

        function build() {
            console.log('bundling...');
            var p = b.bundle()
                .pipe(source('bundle.js'))
                .pipe(buffer());
            if (!watch) {
                p = p.pipe(uglify());
            }

            p.pipe(gulp.dest('./static/'));
        }

        build();
    }
}
gulp.task('watch', getBundler(true));
gulp.task('build', getBundler(false));
gulp.task('test', function () {
    return gulp.src('server/test/**/*.js', {read: false})
        .pipe(mocha({
            reporter: 'nyan'
        }));
});


